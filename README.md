# Cheatsheet Repository 

This repository contains various cybersecurity related cheat sheets for reference.

## Contents

- [Aircrack-ng](aircrack.md) - Cheatsheet for using Aircrack-ng to audit wireless networks.

- [Bettercap](bettercap.md) - Commands for network reconnaissance, sniffing, ARP spoofing, MITM attacks with Bettercap.

- [GPG](gpg.md) - Commands for GPG encryption, decryption, key management, and more.

- [Hashcat](hashcat.md) - Useful Hashcat commands and examples for password cracking.  

- [Nmap](nmap.md) - Cheatsheet for Nmap scanning and scripting.

- [Reformat USB](reformat-usb.md) - Steps for reformatting a USB drive on Linux and Windows.

- [WiFi Handshake](wifi-handshake.md) - Capturing and cracking WiFi handshakes.

These cheat sheets contain commonly used commands and syntax for pentesting tools. They can be used as a quick reference when conducting security assessments and wireless network audits.

The goal is to have a centralized repository to refer to during engagements. Feel free to contribute any additional cheat sheets!
